
var mongoose = require("mongoose");

let mongoUrl;
let options;
mongoose.Promise = global.Promise;

mongoUrl = "mongodb://" + (process.env.MONGO_URI + "/" + process.env.MONGO_DB_NAME);
options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    // user: process.env.MONGO_USERNAME,
    // pass: process.env.MONGO_PASSWORD
  };


// Connecting to the database
mongoose
  .connect(mongoUrl, options)
  .then(() => {
    console.log("Successfully connected to the database", mongoUrl);
  })
  .catch(err => {
    console.error("Could not connect to the database.", err);
    process.exit();
  });
var db = mongoose.connection;

module.exports = db;
