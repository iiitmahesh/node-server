var Movie = require("../models/movie.js");
var async = require("async");

module.exports = {
  create: async (req, res) => {
    let data = req.body;
    console.log(data)
    Movie.create(data, (error, doc) => {
      if (error) {
        res.send({ message: "movie not created", error: error });
      } else {
        res.send({ payload: doc });
      }
    });
  },
  update: async (req, res) => {
    let data = req.body;
    var id = req.params.id;
    // console.log(data,id)
    Movie.findOneAndUpdate({ _id: id}, { $set: req.body }, (error, doc) => {
        console.log(error,doc)

      if (error) {
        res.send({ message: "movie not updated", error: error });
      } else {
        res.send({ payload: doc });
      }
    });
  },
  delete: async (req, res) => {
    var id = req.params.id;
    Movie.remove({ _id: id }, (error, doc) => {
      if (error) {
        res.send({ message: "movie not deleted", error: error });
      } else {
        res.send({ payload: doc });
      }
    });
  },
  get: async (req, res) => {
    let id = req.params.id;
    Movie.findOne({ _id: id }, (error, doc) => {
      if (error) {
        res.send({ message: "movie not found", error: error });
      } else {
        res.send({ payload: doc });
      }
    });
  },
  getAll: async (req, res) => {
    Movie.find({}, (error, docs) => {
      if (error) {
        res.send({ message: "movies not found", error: error });
      } else {
        res.send({ payload: docs });
      }
    });
  }
};
