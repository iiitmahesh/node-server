var mongoose = require("mongoose");

var MovieSchema = new mongoose.Schema(
  {
    name: {
      type: String
    },
    type: {
      type: String
    },
    favorite: {
      type: String
    },
    language: {
      type: String
    },
    rating: {
      type: String
    },
    status: {
      type: String,
      enum: ["ACTIVE", "DELETED"],
      default: "ACTIVE"
    },
  },
  {
    timestamps: true
  }
);

var Movie = mongoose.model("Movie", MovieSchema);

module.exports = Movie;
