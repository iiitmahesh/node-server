
const movieController = require("../controllers/movieController.js");

module.exports = app => {

   app.route('/api/ping').get((req, res) => {
    res.send({
      status: 'OK',
      message: 'pong'
    });
  });


  app.route("/api/movie").post(movieController.create);
  app.route("/api/movie/:id").put(movieController.update);
  app.route("/api/movie/:id").delete(movieController.delete);
  app.route("/api/movie/:id").get(movieController.get);
  app.route("/api/movies/getAll").get(movieController.getAll);

};
