var express = require("express");
require("dotenv").config();
var port = process.env.PORT ? process.env.PORT:3000;
var bodyParser = require("body-parser");
var cors = require("cors");
//connect to MongoDB
global.db = require("./config/db.js");

var app = express();
app.use(bodyParser.json({ limit: "100mb" }));
app.set("view engine", "pug");
app.use(cors());

require("./api/routes/router.js")(app);

app.listen(port);
console.log("listening on " + port);
